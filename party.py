# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction

class Address(ModelSQL, ModelView):
    _name = 'party.address'

    site_operation = fields.Boolean('Site of Operation')

    def default_party(self):
        return Transaction().context.get('party', False)

Address()

