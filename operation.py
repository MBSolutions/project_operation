# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.utils import formatdate, make_msgid
from string import Template
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields
from trytond.pyson import Eval, Bool, Equal, Not, In
from trytond.transaction import Transaction
from trytond.wizard import Wizard
from trytond.tools import get_smtp_server
from trytond.pool import Pool


# TODO: use mimetypes python module
_EXT2MIME = {
    'odt': 'vnd.oasis.opendocument.text',
    'ods': 'vnd.oasis.opendocument.spreadsheet',
    'odp': 'vnd.oasis.opendocument.presentation',
    'pdf': 'pdf',
}


class Operation(ModelWorkflow, ModelSQL, ModelView):
    'Project Operation'
    _name = 'project.operation'
    _description = __doc__
    _rec_name = 'code'

    code = fields.Char('Code', readonly=True)
    contact_person = fields.Many2One('party.party', 'Contact Person',
            required=True, domain=[
                ('party_type', '=', 'person'),
                ('organization_members.organization', 'in', [Eval('party'),]),
            ], states={
                    'readonly': Not(Equal(Eval('state'), 'draft')),
            }, depends=['party', 'state'])
    party = fields.Many2One('party.party', 'Party', required=True,
            on_change=['party', 'contact_person', 'location_address'],
            domain=[('party_type', '=', 'organization')],
            states={
                    'readonly': Not(Equal(Eval('state'), 'draft')),
            }, depends=['state'])
    project_work = fields.Many2One('project.work', 'Project Work', domain=[
                    ('party', '=', Eval('party')),
                    ('type', '=', 'project'),
            ], context={
                    'party': Eval('party'),
                    'type': 'project',
            }, states={
                'readonly':  Not(Equal(Eval('state'), 'draft')),
                'required': True,
            }, depends=['party', 'state'])
    work = fields.Function(fields.Many2One('timesheet.work', 'Work'), 'get_work')
    timesheet_lines = fields.One2Many('timesheet.line', 'operation',
            'Timesheet Lines', states={
                'readonly': Not(Equal(Eval('state'), 'draft')),
            }, depends=['project_work', 'state', 'work'])
    work_description = fields.Text('Work Description', states={
                'readonly': Not(Equal(Eval('state'), 'draft')),
                'required': Not(In(Eval('state'), ['draft', 'cancel']))
            }, depends=['state'])
    state = fields.Selection([
                ('draft', 'Draft'),
                ('confirmed', 'Confirmed'),
                ('cancel', 'Canceled')
            ], 'State', readonly=True)
    location_address = fields.Many2One('party.address', 'Location Address',
            domain=[
                ('party', '=', Eval('party')),
            ], context={
                'party': Eval('party'),
            }, states={
                'readonly': Not(Equal(Eval('state'), 'draft')),
                'required': Bool(Equal(Eval('type'), 'on-site')),
            }, depends=['party', 'state', 'type'])
    company = fields.Many2One('company.company', 'Company', required=True,
            states={
                'readonly': Not(Equal(Eval('state'), 'draft')),
            }, depends=['state'])
    total_hours = fields.Function(fields.Float('Total Hours', digits=(16,2)),
            'get_total_hours')
    confirm_date = fields.Date('Confirmation Date')

    def __init__(self):
        super(Operation, self).__init__()
        self._error_messages.update({
            'delete_operation': "You cannot delete a operation that "
                "has a state different to 'Canceled'!"
            })
        self._rpc.update({
            'button_draft': True,
        })

    def default_state(self):
        return 'draft'

    def default_project_work(self):
        return Transaction().context.get('project_work', False)

    def default_work(self):
        project_obj = Pool().get('project.work')

        res = False
        project_work_id = Transaction().context.get('project_work', False)
        if project_work_id:
            project = project_obj.browse(project_work_id)
            res = project.work.id
        return res

    def default_party(self):
        return Transaction().context.get('party', False)

    def default_location_address(self):
        party_obj = Pool().get('party.party')
        if Transaction().context.get('party'):
            party = party_obj.browse(Transaction().context['party'])
            if len(party.addresses) == 1:
                return party_obj.address_get(Transaction().context['party'])
        return False

    def default_contact_person(self):
        party_obj = Pool().get('party.party')
        if Transaction().context.get('party'):
            party = party_obj.browse(Transaction().context['party'])
            if len(party.person_members) == 1:
                return party.person_members[0].id
        return False

    def default_company(self):
        return Transaction().context.get('company') or False

    def on_change_party(self, vals):
        party_obj = Pool().get('party.party')
        res = {}
        if not vals.get('party'):
            res['contact_person'] = False
            res['location_address'] = False
            res['party'] = False
        else:
            party = party_obj.browse(vals['party'])
            if not vals.get('contact_person'):
                if len(party.person_members) == 1:
                    res['contact_person'] = party.person_members[0].id
            if not vals.get('location_address'):
                if len(party.addresses) == 1:
                    res['location_address'] = party.addresses[0].id
        return res

    def get_work(self, ids, name):
        res = {}
        if not ids:
            return res
        for operation in self.browse(ids):
            res[operation.id] = operation.project_work.work.id
        return res

    def get_total_hours(self, ids, name):
        res = {}
        if not ids:
            return res
        for operation in self.browse(ids):
            hours = 0
            for line in operation.timesheet_lines:
                hours += line.hours
            res[operation.id] = hours
        return res

    def button_draft(self, ids):
        operations = self.browse(ids)
        self.workflow_trigger_create([x.id for x in operations])
        self.write(ids, {'state': 'draft'})
        return True

    def set_code(self, operation_id):
        sequence_obj = Pool().get('ir.sequence')
        operation = self.browse(operation_id)
        if operation.code:
            return True

        vals = {'code': sequence_obj.get('project.operation')}
        self.write(operation.id, vals)
        return True

    def copy(self, ids, default=None):
        int_id = False
        if isinstance(ids, (int, long)):
            int_id = True
            ids = [ids]
        if default is None:
            default = {}
        default = default.copy()
        default['code'] = False
        new_ids = []
        for operation in self.browse(ids):
            new_id = super(Operation, self).copy(operation.id, default=default)
            new_ids.append(new_id)
        if int_id:
            return new_ids[0]
        return new_ids

    def check_delete(self, ids, raise_exception=False):
        '''
        Check if the operation can be deleted
        '''
        if isinstance(ids, (int, long)):
            ids = [ids]
        for operation in self.browse(ids):
            if operation.state != 'cancel':
                if raise_exception:
                    self.raise_user_error('delete_operation')
                return False
        return True

    def delete(self, ids):
        self.check_delete(ids, raise_exception=True)
        return super(Operation, self).delete(ids)

    def wkf_operation_activity_draft(self, operation):
        self.set_code(operation.id)
        self.write(operation.id, {'state': 'draft'})

    def wkf_operation_activity_cancel(self, operation):
        self.write(operation.id, {'state': 'cancel'})

    def wkf_operation_activity_confirm(self, operation):
        date_obj = Pool().get('ir.date')
        self.write(operation.id, {
            'confirm_date': date_obj.today(),
            'state': 'confirmed',
            })

Operation()


class ConfirmInit(ModelView):
    'Confirm Init'
    _name = 'project.operation.wizard_confirm.init'
    _description = __doc__

    operation = fields.Many2One('project.operation', 'Operation',
        states={'invisible': True})
    party = fields.Many2One('party.party', 'Partei',
        domain=[('party_type', '=', 'organisation')], readonly=True)
    contact_person = fields.Many2One('party.party', 'Contact Person',
        on_change=['contact_person', 'email', 'operation'],
        domain=[
            ('party_type', '=', 'person'),
            ('organization_members.organization', 'in', [Eval('party'),]),
        ], depends=['party'])
    email = fields.Char('E-Mail', required=True,
        on_change=['contact_person', 'email', 'operation'])
    subject = fields.Char('Subject', required=True)
    message = fields.Text('Message', required=True)

    def on_change_contact_person(self, vals):
        party_obj = Pool().get('party.party')
        config_obj = Pool().get('project.configuration')
        config = config_obj.browse(config_obj.get_singleton_id())
        res = {'email': False}
        if vals.get('contact_person'):
            contact_person = party_obj.browse(vals['contact_person'])
            res['email'] = contact_person.email
        if config.operation_email_message:
            mapping = self._get_mapping(vals)
            template = Template(config.operation_email_message)
            res['message'] = template.substitute(mapping)
        return res

    def on_change_email(self, vals):
        config_obj = Pool().get('project.configuration')
        config = config_obj.browse(config_obj.get_singleton_id())
        res = {}
        if not vals.get('email') or vals.get('contact_person'):
            vals2 = vals.copy()
            vals2['contact_person'] = False
            res['contact_person'] = False
            if config.operation_email_message:
                mapping = self._get_mapping(vals2)
                template = Template(config.operation_email_message)
                res['message'] = template.substitute(mapping)
        return res

    def _get_mapping(self, vals):
        pool = Pool()
        operation_obj = pool.get('project.operation')
        user_obj = pool.get('res.user')
        salutation_obj = pool.get('party.salutation')

        operation = operation_obj.browse(vals['operation'])
        user = user_obj.browse(Transaction().user)

        res = {
            'operation_code': operation.code or '',
            'project_name': operation.project_work.rec_name or '',
            'salutation': salutation_obj.get_salutation(
                vals['contact_person']),
            'signature': user.signature or '',
            }
        return res

ConfirmInit()


class Confirm(Wizard):
    'Confirm Operation'
    _name = 'project.operation.wizard_confirm'

    states = {
        'init': {
            'actions': ['_init'],
            'result': {
                'type': 'form',
                'object': 'project.operation.wizard_confirm.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('confirm_send', 'Confirm + Send', 'tryton-mail-message'),
                ],
            },
        },
        'confirm_send': {
            'result': {
                'type': 'action',
                'action': '_action_confirm_send',
                'state': 'end',
            },
        },
        'confirm': {
            'result': {
                'type': 'action',
                'action': '_action_confirm',
                'state': 'end',
            },
        },
    }

    def __init__(self):
        super(Confirm, self).__init__()
        self._error_messages.update({
            'missing_email': "No email address defined for this user. "
                "Please define one in the user preferences.",
            'smtp_error': "SMTP Error: Can not send email.\n\n"
                "Details:\n\n%s"
            })

    def _init(self, data):
        config_obj = Pool().get('project.configuration')
        operation_obj = Pool().get('project.operation')

        config = config_obj.browse(config_obj.get_singleton_id())
        operation = operation_obj.browse(data['id'])
        subject = self._parse_text(config.operation_email_subject, data)
        message = self._parse_text(config.operation_email_message, data)
        return {
            'operation': operation.id,
            'party': operation.party.id,
            'contact_person': operation.contact_person.id,
            'email': operation.contact_person.email,
            'subject': subject,
            'message': message,
            }

    def _parse_text(self, text, data):
        res = ''
        if text:
            mapping = self._get_mapping(data)
            template = Template(text)
            res = template.substitute(mapping)
        return res

    def _get_mapping(self, data):
        pool = Pool()
        operation_obj = pool.get('project.operation')
        user_obj = pool.get('res.user')
        salutation_obj = pool.get('party.salutation')

        operation = operation_obj.browse(data['id'])
        user = user_obj.browse(data['user'])

        res = {
            'operation_code': operation.code or '',
            'project_name': operation.project_work.rec_name or '',
            'salutation': salutation_obj.get_salutation(
                operation.contact_person),
            'signature': user.signature or '',
            }
        return res

    def _action_confirm_send(self, data):
        self._action_confirm(data)
        self._action_send(data)

    def _action_confirm(self, data):
        operation_obj = Pool().get('project.operation')
        operation = operation_obj.browse(data['id'])
        operation.workflow_trigger_validate(data['id'], 'confirm')
        return {}

    def _action_send(self, data):
        operation_report = Pool().get('project.operation.report',
            type='report')

        from_address = self._get_from_address(data)
        to_address = self._get_to_address(data)
        report_data = operation_report.execute([data['id']],
            {'id': data['id']})

        msg = MIMEMultipart()
        subject = data['form']['subject']
        msg['Subject'] = Header(subject, 'utf-8')
        msg['From'] = from_address
        msg['To'] = to_address
        msg['Date'] = formatdate()
        msg['Message-ID'] = make_msgid()

        message = data['form']['message']
        msg.attach(MIMEText(message, _charset='utf-8'))
        part = MIMEApplication(report_data[1],
            _EXT2MIME[report_data[0]])
        part.add_header('Content-Disposition',
            'attachment; filename="%s"' % (
                report_data[3] + '.' + report_data[0],
                )
            )
        msg.attach(part)

        smtp_server = get_smtp_server()
        res = smtp_server.sendmail(
            from_address,
            to_address,
            msg.as_string())
        if res:
            self.raise_user_error('smtp_error', error_args=(res,))
        return {}

    def _get_from_address(self, data):
        user_obj = Pool().get('res.user')

        user = user_obj.browse(data['user'])
        if not user.email:
            self.raise_user_error('missing_email')
        realname = False
        if user.employee:
            realname = user.employee.full_name
        return self._format_address(user.email, realname)

    def _format_address(self, email, name=False):
        # TODO: Make this IDNA aware
        res = str(email)
        if name:
            hname = Header(name, 'utf-8')
            res = str(hname) + ' <' + res + '>'
        return res

    def _get_to_address(self, data):
        party_obj = Pool().get('party.party')

        email = data['form']['email']
        realname = False
        if data['form']['contact_person']:
            contact_person = party_obj.browse(data['form']['contact_person'])
            if email == contact_person.email:
                realname = contact_person.full_name
        return self._format_address(email, realname)

Confirm()
